//
//  ViewController.m
//  Web_View_Xibs
//
//  Created by Bruno Tavares on 22/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;
    
    NSString *urlString = @"https://www.betfair.com/exchange/";
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    [self.view addSubview:self.loadingLabel];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [self.loadingLabel setHidden:false];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.loadingLabel setHidden:true];
}

@end
